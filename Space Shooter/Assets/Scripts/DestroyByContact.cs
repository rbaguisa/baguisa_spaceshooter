﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour
{
	public GameObject explosion;
	public GameObject playerExplosion;
	public int scoreValue;
	private GameController gameController;

	void Start ()
	{
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null) 
		{
			gameController = gameControllerObject.GetComponent <GameController> ();
		}
		if (gameController == null) 
		{
			Debug.Log ("Cannot find 'GameController' script");
		}
	}

	void OnTriggerEnter(Collider other)
	{
//		if (other.tag == "Boundary") 

		if (	
			other.CompareTag ("Boundary") ||
			other.CompareTag ("Enemy") ||
			other.CompareTag ("EnemyShot")	||
			(gameObject.CompareTag("EnemyShot") && other.CompareTag ("PlayerShot"))
		)		
		
		{
			return;//Don't destroy when running above tags
		}



		if (explosion != null) {
			Instantiate (explosion, transform.position, transform.rotation);
		}
	
		if (other.tag == "Player") {
			
			Instantiate (playerExplosion, other.transform.position, other.transform.rotation);
			gameController.GameOver ();
		}

		gameController.AddScore (scoreValue);

		//if (other.tag != "Shield")
		{
			Destroy (other.gameObject);
		}
	

		Destroy (gameObject);
	}
}